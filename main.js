"use strict"

let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, dataType) {
    return arr.filter((data) => typeof data !== dataType);
}

let filteredArr = filterBy(arr, 'string');
console.log(filteredArr);
